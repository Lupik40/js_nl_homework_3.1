/* 
    TASK 1

    Дан объект : 

    const car = {
        company : 'Toyota',
        model : 'Land Cruser',
        doors : 5,
        color : 'white'
    }

    Необходимо преобразовать данный объект в формат JSON , и потом обратно.
    Запишите оба результата в переменную и выведите их значения в консоль.

*/

const car = {
    company: 'Toyota',
    model: 'Land Cruser',
    doors: 5,
    color: 'white'
};

const carJson = JSON.stringify(car);
console.log(carJson);
const carJsonObj = JSON.parse(carJson);
console.log(carJsonObj);

/* 
    TASK 2

    Воспользуйтесь free REST API: https://jsonplaceholder.typicode.com/ для получения 
    100 albums. И выведите все альбомы на html страницу в виде : 

    UserId : значение userId с пришедшего вам объекта,
    Id : значение Id с пришедшего вам объекта,
    Title : значение title с пришедшего вам объекта

    В итоге на вашей странице должно распарситься 100 разных альбомов. 

*/

const albom = document.getElementById('albom');

const xhr = new XMLHttpRequest();
const url = 'https://jsonplaceholder.typicode.com/albums';

xhr.open('GET', url);

xhr.addEventListener('load', () => {
    let alboms = JSON.parse(xhr.response);

    alboms.forEach(item => {
        let albomItem = document.createElement('div');

        albomItem.innerHTML =
        `
            <p id="userId">UserID : ${item.userId}</p>
            <p id="id">Id : ${item.id}</p>
            <p id="userTitle">Title : ${item.title}</p>
        `;

        albom.append(albomItem);
    });
});

xhr.send();